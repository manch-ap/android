# Manch AP
Manch Assignment (Android Client)

## Node.js Server
[Repo](https://gitlab.com/manch-ap/server)

## Features
    1. User
        1. Login
        2. Signup
    2. Posts
        1. Creation
        2. Timeline
    3. Following
        1. Follow
        2. Unfollow

### Notes
    1. This is a pretty basic implementation
    2. No Serious Input Validation
    3. No Caching (Emphasis on Basic Implementation)