package com.example.manchap.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.example.manchap.api.Api;
import com.example.manchap.api.BaseObserver;
import com.example.manchap.api.model.NewPost;
import com.example.manchap.api.model.Posts;
import com.example.manchap.model.Following;
import com.example.manchap.model.Post;
import com.example.manchap.util.TimelinePager;

public class TimelineModel {
    private Api api = Api.getInstance();
    private TimelinePager pager;

    private MutableLiveData<Boolean> processing = new MutableLiveData<>();
    private MutableLiveData<String> postText = new MutableLiveData<>();

    public TimelineModel(String user) {
        if (user != null) {
            pager = new TimelinePager(user);
        } else {
            pager = new TimelinePager();
        }
        processing.setValue(false);
    }

    public MutableLiveData<Boolean> getProcessing() {
        return processing;
    }

    public MutableLiveData<String> getPostText() {
        return postText;
    }

    public void setPages(int pages) {
        pager.pages = pages;
    }

    /**
     * API Calls
     */
    // Initial Call
    public void firstPage(BaseObserver<Posts> postsBaseObserver) {
        pager.reset();
        nextPage(postsBaseObserver);
    }

    // Next Page
    public void nextPage(BaseObserver<Posts> postsBaseObserver) {
        if (!pager.hasMore()) {
            return;
        }
        pager.setPage(pager.page + 1);
        api.getPosts(pager, postsBaseObserver);
    }

    // Create Post
    public void createPost(BaseObserver<Post> observer) {
        api.createPost(new NewPost(postText.getValue()), observer);
    }

    // Follow
    public void follow(String userid, BaseObserver<Following> observer) {
        api.follow(userid, observer);
    }

    // Unfollow
    public void unfollow(String userid, BaseObserver<Following> observer) {
        api.unfollow(userid, observer);
    }
}
