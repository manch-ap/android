package com.example.manchap.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.example.manchap.api.Api;
import com.example.manchap.api.BaseObserver;
import com.example.manchap.api.model.Signup;
import com.example.manchap.model.User;

public class SignupModel {

    private MutableLiveData<String> name = new MutableLiveData<>();
    private MutableLiveData<String> email = new MutableLiveData<>();
    private MutableLiveData<String> password = new MutableLiveData<>();

    // API Calls
    private Api api = Api.getInstance();

    public SignupModel() {
        name.setValue(null);
        email.setValue(null);
        password.setValue(null);
    }

    public MutableLiveData<String> getName() {
        return name;
    }

    public MutableLiveData<String> getEmail() {
        return email;
    }

    public MutableLiveData<String> getPassword() {
        return password;
    }

    /**
     * API Calls
     */
    // Signup
    public void signup(BaseObserver<User> observer) {
        api.signup(new Signup(email.getValue(), name.getValue(), password.getValue()), observer);
    }
}
