package com.example.manchap.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.example.manchap.api.Api;
import com.example.manchap.api.BaseObserver;
import com.example.manchap.api.model.Login;
import com.example.manchap.model.User;

public class LoginModel {
    private MutableLiveData<String> email = new MutableLiveData<>();
    private MutableLiveData<String> password = new MutableLiveData<>();

    // API Calls
    private Api api = Api.getInstance();

    public LoginModel() {
        email.setValue(null);
        password.setValue(null);
    }

    public MutableLiveData<String> getEmail() {
        return email;
    }

    public MutableLiveData<String> getPassword() {
        return password;
    }

    /**
     * API Calls
     */
    // Login
    public void login(BaseObserver<User> observer) {
        api.login(new Login(email.getValue(), password.getValue()), observer);
    }
}
