package com.example.manchap.adapter.holder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.example.manchap.R;
import com.example.manchap.adapter.PostsAdapter;
import com.example.manchap.model.Post;
import com.example.manchap.module.FollowingCache;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostHolder extends RecyclerView.ViewHolder {

    private static final SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
    private static final FollowingCache followingCache = FollowingCache.getInstance();

    @BindView(R.id.rl_profile) View profile;
    @BindView(R.id.tv_name) TextView name;
    @BindView(R.id.tv_time_elapsed) TextView timeElapsed;
    @BindView(R.id.bt_follow) AppCompatButton follow;
    @BindView(R.id.tv_text) TextView text;

    public PostHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void update(final int position, final Post post, final PostsAdapter.ClickListener clickListener) {
        // Set Data
        name.setText(post.user.name);
        timeElapsed.setText(calculateTimeElapsed(post.createdAt));
        text.setText(post.text);

        // Set Listeners
        profile.setOnClickListener(v -> {
            clickListener.onProfileClicked(position, post);
        });

        follow.setOnClickListener(v -> {
            clickListener.onFollowClicked(position, post);
        });

        // Hide follow for current user
        follow.setVisibility(post.user.self ? View.GONE : View.VISIBLE);
        follow.setText(followingCache.get(post.user.id) ? R.string.unfollow : R.string.follow);
    }

    private static String calculateTimeElapsed(String date) {
        try {
            inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return new PrettyTime().format(inputFormat.parse(date));
        } catch (Exception e) {
            return "-";
        }
    }
}
