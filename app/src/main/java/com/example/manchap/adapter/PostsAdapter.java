package com.example.manchap.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.manchap.R;
import com.example.manchap.adapter.holder.PostHolder;
import com.example.manchap.model.Post;

import java.util.ArrayList;
import java.util.List;


public class PostsAdapter extends RecyclerView.Adapter<PostHolder> {
    private Context context;
    private ArrayList<Post> posts;
    private ClickListener clickListener = new ClickListener() {
        @Override
        public void onFollowClicked(int position, Post post) {

        }

        @Override
        public void onProfileClicked(int position, Post post) {

        }
    };

    public PostsAdapter(Context context) {
        this.context = context;
        this.posts = new ArrayList<>();
    }

    @NonNull
    @Override
    public PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PostHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PostHolder holder, int position) {
        holder.update(position, posts.get(position), clickListener);
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    // Modifications
    public void add(Post post) {
        posts.add(post);
        notifyItemInserted(posts.size() - 1);
    }

    public void addFront(Post post) {
        posts.add(0, post);
        notifyItemInserted(0);
    }

    public void addAll(List<Post> posts) {
        final int lastPosition = this.posts.size();
        this.posts.addAll(posts);
        notifyItemRangeInserted(lastPosition, posts.size());
    }

    public void remove(int position) {
        if(position < 0 || position >= getItemCount()) {
            return;
        }
        posts.remove(position);
        notifyDataSetChanged();
    }

    public void clearAll() {
        posts.clear();
        notifyDataSetChanged();
    }

    // Listeners
    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onFollowClicked(int position, Post post);
        void onProfileClicked(int position, Post post);
    }
}
