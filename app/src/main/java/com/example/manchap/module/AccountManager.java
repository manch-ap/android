package com.example.manchap.module;

import android.content.SharedPreferences;

import com.example.manchap.App;
import com.example.manchap.model.User;

public class AccountManager {
    private static AccountManager instance;

    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    private AccountManager() {
        if (instance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
        sp = App.getInstance().getSharedPreferences("account", 0);
        editor = sp.edit();
    }

    public static AccountManager getInstance() {
        if (instance == null) {
            instance = new AccountManager();
        }
        return instance;
    }

    public void setUser(User user) {
        editor.putString("user", user.toJSONString());
        editor.commit();
    }

    public User getUser() {
        return User.parseJSONString(sp.getString("user", null));
    }

    public void clear() {
        editor.putString("user", null);
        editor.commit();
    }
}
