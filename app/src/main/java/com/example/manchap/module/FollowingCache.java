package com.example.manchap.module;

import com.ufkoku.cache.SizeBasedEnhancedLruCache;

public class FollowingCache {

    private static final int CACHE_SIZE = 500 * 1024;
    private static FollowingCache instance;

    private SizeBasedEnhancedLruCache<String, Boolean> cache;

    public static FollowingCache getInstance() {
        if (instance == null) {
            instance = new FollowingCache();
        }
        return instance;
    }

    private FollowingCache() {
        cache = new SizeBasedEnhancedLruCache<>(CACHE_SIZE);
    }

    public void set(String userId, boolean value) {
        cache.put(userId, value);
    }

    public Boolean get(String userId) {
        return cache.get(userId);
    }
}
