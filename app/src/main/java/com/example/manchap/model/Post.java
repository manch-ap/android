package com.example.manchap.model;

public class Post {
    public String id;
    public User user;
    public String text;
    public String createdAt;
    public String updatedAt;
}
