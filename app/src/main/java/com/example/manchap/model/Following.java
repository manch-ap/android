package com.example.manchap.model;

public class Following {
    public String id;
    public String user;
    public String follower;
    public String createdAt;
    public String updatedAt;
}
