package com.example.manchap.model;

import com.example.manchap.api.model.Follower;
import com.google.gson.Gson;

public class User {
    public String token;
    public String id;
    public String name;
    public String email;
    public Boolean self = false;
    public Follower following;

    public User(String token, String id, String name, String email) {
        this.token = token;
        this.id = id;
        this.name = name;
        this.email = email;
    }

    // Parsing
    public static User parseJSONString(String userJSONString) {
        if(userJSONString == null) return null;
        return new Gson().fromJson(userJSONString, User.class);
    }

    // Stringify
    public String toJSONString() {
        return new Gson().getAdapter(User.class).toJson(this);
    }
}
