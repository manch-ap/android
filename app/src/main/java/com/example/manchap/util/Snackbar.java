package com.example.manchap.util;

import android.view.View;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.example.manchap.R;

public class Snackbar {
    public static void show(@NonNull android.view.View view, @ColorRes int backgroundColor, @NonNull String message) {
        com.google.android.material.snackbar.Snackbar snackbar = com.google.android.material.snackbar.Snackbar.make(view,
                message,
                com.google.android.material.snackbar.Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundResource(backgroundColor);
        snackbar.show();
    }

    public static void show(@NonNull android.view.View view, @ColorRes int backgroundColor, @StringRes int message) {
        com.google.android.material.snackbar.Snackbar snackbar = com.google.android.material.snackbar.Snackbar.make(view,
                message,
                com.google.android.material.snackbar.Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundResource(backgroundColor);
        snackbar.show();
    }

    public static com.google.android.material.snackbar.Snackbar show(@NonNull android.view.View view, @ColorRes int backgroundColor, @StringRes int message, @StringRes int action, View.OnClickListener actionClickListener) {
        com.google.android.material.snackbar.Snackbar snackbar = com.google.android.material.snackbar.Snackbar.make(view,
                message,
                com.google.android.material.snackbar.Snackbar.LENGTH_INDEFINITE);
        snackbar.getView().setBackgroundResource(backgroundColor);
        snackbar.setActionTextColor(view.getContext().getResources().getColor(R.color.text_normal_white));
        snackbar.setAction(action, actionClickListener);
        snackbar.show();
        return snackbar;
    }
}
