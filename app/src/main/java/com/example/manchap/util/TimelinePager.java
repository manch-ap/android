package com.example.manchap.util;

import android.content.Intent;

public class TimelinePager {
    public int mode;
    public String user;
    public int page = 0;
    public int size = 20;
    public int sort = -1;
    public int pages = -1;

    public TimelinePager() {
        this.mode = 0;
    }

    public TimelinePager(String user) {
        this.mode = 1;
        this.user = user;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void reset() {
        page = 0;
        pages = -1;
    }

    public boolean hasMore() {
        return pages < 0 || page < pages;
    }
}
