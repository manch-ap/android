package com.example.manchap.api;

import android.net.ParseException;

import com.google.gson.JsonParseException;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;

import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class ExceptionHandler {
    private static final int UNAUTHORIZED = 401;
    private static final int FORBIDDEN = 403;
    private static final int NOT_FOUND = 404;
    private static final int REQUEST_TIMEOUT = 408;
    private static final int INTERNAL_SERVER_ERROR = 500;
    private static final int BAD_GATEWAY = 502;
    private static final int SERVICE_UNAVAILABLE = 503;
    private static final int GATEWAY_TIMEOUT = 504;

    public static ResponseThrowable handleException(Throwable e) {
        ResponseThrowable ex;
        if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;
            ResponseBody errorBody = httpException.response().errorBody();

            //Create Throwable
            ex = new ResponseThrowable(e, ErrorCodes.UNKNOWN);
            ex.httpCode = httpException.code();

            //Check if API error
            if(errorBody != null &&
                    errorBody.contentType() != null &&
                    errorBody.contentType().toString().toLowerCase().contains("application/json")) {
                ex.code = ErrorCodes.API_ERROR;
                try {
                    JSONObject apiError = new JSONObject(errorBody.string());
                    ex.message = apiError.optString("message");
                    ex.additionalInfo = apiError.optString("additionalInfo");
                } catch (Exception ignored) {
                    ex.message = "Error parsing API_ERROR";
                }
            } else {
                ex.code = ErrorCodes.HTTP_ERROR;
                switch (ex.httpCode) {
                    case UNAUTHORIZED:
                    case FORBIDDEN:
                    case NOT_FOUND:
                    case REQUEST_TIMEOUT:
                    case GATEWAY_TIMEOUT:
                    case INTERNAL_SERVER_ERROR:
                    case BAD_GATEWAY:
                    case SERVICE_UNAVAILABLE:
                    default:
                        ex.message = "Network Error";
                        break;
                }
            }
        } else if (e instanceof JsonParseException
                || e instanceof JSONException
                || e instanceof ParseException) {
            ex = new ResponseThrowable(e, ErrorCodes.PARSE_ERROR);
            ex.message = "Parsing Json Error";
        } else if (e instanceof ConnectException) {
            ex = new ResponseThrowable(e, ErrorCodes.NETWORK_ERROR);
            ex.message = "Connect Error";
        } else if (e instanceof javax.net.ssl.SSLHandshakeException) {
            ex = new ResponseThrowable(e, ErrorCodes.SSL_ERROR);
            ex.message = "SSL Error";
        } else if (e instanceof ConnectTimeoutException){
            ex = new ResponseThrowable(e, ErrorCodes.TIMEOUT_ERROR);
            ex.message = "Connect Timeout Error";
        } else if (e instanceof java.net.SocketTimeoutException) {
            ex = new ResponseThrowable(e, ErrorCodes.TIMEOUT_ERROR);
            ex.message = "Socket Timeout Error";
        } else {
            ex = new ResponseThrowable(e, ErrorCodes.UNKNOWN);
            ex.message = "Unknown Error";
        }
        return ex;
    }

    public static class ErrorCodes {
        public static final int UNKNOWN = 1000;
        public static final int PARSE_ERROR = 1001;
        public static final int NETWORK_ERROR = 1002;
        public static final int HTTP_ERROR = 1003;
        public static final int SSL_ERROR = 1005;
        public static final int TIMEOUT_ERROR = 1006;
        public static final int API_ERROR = 1007;
    }

    public static class ResponseThrowable extends Exception {
        public int code;
        public int httpCode = -1;
        public String message;
        public String additionalInfo;

        public ResponseThrowable(Throwable throwable, int code) {
            super(throwable);
            this.code = code;
        }

        @Override
        public String toString() {
            return "HttpCode : " + httpCode + " :: Code : " + code + " :: Message : " + message + " :: AdditionalInfo : " + additionalInfo;
        }
    }
}