package com.example.manchap.api.model;

import com.example.manchap.model.Post;

import java.util.ArrayList;

public class Posts {
    public ArrayList<Post> posts = new ArrayList<>();
    public Integer total;
    public Integer limit;
    public Integer current;
    public Integer next;
    public Integer prev;
    public Integer pages;
    public Integer pagingCounter;
}
