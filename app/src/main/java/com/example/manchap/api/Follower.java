package com.example.manchap.api;

import com.example.manchap.model.Following;

import io.reactivex.Observable;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface Follower {
    String BASE_URL = "/follower";

    // Follow
    @PUT(BASE_URL + "/follow/{userid}")
    Observable<Following> follow(@Path("userid") String userid);

    // Unfollow
    @DELETE(BASE_URL + "/unfollow/{userid}")
    Observable<Following> unfollow(@Path("userid") String userid);

    // Follow Status
    @GET(BASE_URL + "/following/{userid}")
    Observable<com.example.manchap.api.model.Follower> status(@Path("userid") String userid);
}
