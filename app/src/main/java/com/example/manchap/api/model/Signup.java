package com.example.manchap.api.model;

public class Signup {
    public String name;
    public String password;
    public String email;

    public Signup(String email, String name, String password) {
        this.email = email;
        this.name = name;
        this.password = password;
    }
}
