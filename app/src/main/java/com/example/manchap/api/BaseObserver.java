package com.example.manchap.api;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public abstract class BaseObserver<T> implements Observer<T> {

    @Override
    public void onSubscribe(Disposable d) {
        showDialog();
    }

    @Override
    public void onError(Throwable e) {
        hideDialog();
        if (e instanceof ExceptionHandler.ResponseThrowable) {
            onError((ExceptionHandler.ResponseThrowable) e);
        } else {
            onError(new ExceptionHandler.ResponseThrowable(e, ExceptionHandler.ErrorCodes.UNKNOWN));
        }
    }

    @Override
    public void onNext(T result) {
        hideDialog();
        onResult(result);
    }

    @Override
    public void onComplete() {
        hideDialog();
    }

    public abstract void hideDialog();

    public abstract void showDialog();

    public abstract void onError(ExceptionHandler.ResponseThrowable e);

    public abstract void onResult(T result);
}