package com.example.manchap.api;

import com.example.manchap.api.model.NewPost;
import com.example.manchap.api.model.Posts;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface Post {
    String BASE_URL = "/post";

    // Create
    @PUT(BASE_URL)
    Observable<com.example.manchap.model.Post> create(@Body NewPost newPost);

    // List
    @GET(BASE_URL + "/timeline")
    Observable<Posts> timeline(@Query("mode") int mode, @Query("page") int page, @Query("sort") int sort, @Query("size") int size, @Query("user") String user);
}
