package com.example.manchap.api;

import com.example.manchap.api.model.Login;
import com.example.manchap.api.model.NewPost;
import com.example.manchap.api.model.Posts;
import com.example.manchap.api.model.Signup;
import com.example.manchap.model.Following;
import com.example.manchap.model.User;
import com.example.manchap.module.AccountManager;
import com.example.manchap.util.TimelinePager;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {
    final static String API_URL = "https://manch-ap.herokuapp.com/";
    final static String HEADER_TOKEN = "X-Access-Token";
    final static int TIMEOUT = 30;

    private ObservableTransformer schedulersTransformer = upstream -> upstream.subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
    private static Api api;
    private AccountManager accountManager;
    private com.example.manchap.api.User userApi;
    private Post postApi;
    private Follower followerApi;

    //Instance
    public static Api getInstance() {
        if (api == null) {
            api = new Api();
        }
        return api;
    }


    private Api() {
        // Fetch Account Manager
        accountManager = AccountManager.getInstance();

        // Create Common Client
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(TIMEOUT, TimeUnit.SECONDS);
        builder.addNetworkInterceptor(chain -> {
            User user = accountManager.getUser();
            Request request = chain.request();
            request = request.newBuilder()
                    .addHeader(HEADER_TOKEN, user != null ? user.token : "")
                    .build();
            return chain.proceed(request);
        });
        builder.addNetworkInterceptor(chain -> {
            Request request = chain.request();
            Response response = chain.proceed(request);
            if(response.header(HEADER_TOKEN) != null) {
                User user = accountManager.getUser();
                if (user == null) {
                    user = new User("", "", "", "");
                }
                user.token = response.header(HEADER_TOKEN);
                accountManager.setUser(user);
            }
            return response;
        });

        // User API
        userApi = new Retrofit.Builder()
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API_URL)
                .build()
                .create(com.example.manchap.api.User.class);
        // Posts API
        postApi = new Retrofit.Builder()
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API_URL)
                .build()
                .create(Post.class);
        // Follower API
        followerApi = new Retrofit.Builder()
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API_URL)
                .build()
                .create(Follower.class);
    }

    /**
     * User API
     */
    public void signup(Signup signup, BaseObserver<User> observer) {
        subscribe(userApi.signup(signup), observer);
    }

    public void login(Login login, BaseObserver<User> observer) {
        subscribe(userApi.login(login), observer);
    }

    /**
     * Posts API
     */
    public void createPost(NewPost newPost, BaseObserver<com.example.manchap.model.Post> observable) {
        subscribe(postApi.create(newPost), observable);
    }

    public void getPosts(TimelinePager pager, BaseObserver<Posts> observer) {
        subscribe(postApi.timeline(pager.mode, pager.page, pager.sort, pager.size, pager.user), observer);
    }

    /**
     * Follower API
     */
    public void follow(String user, BaseObserver<Following> observer) {
        subscribe(followerApi.follow(user), observer);
    }

    public void unfollow(String user, BaseObserver<Following> observer) {
        subscribe(followerApi.unfollow(user), observer);
    }

    public void followingStatus(String user, BaseObserver<com.example.manchap.api.model.Follower> observer) {
        subscribe(followerApi.status(user), observer);
    }

    /**
     * Util
     */
    private <T> void subscribe(Observable<T> observable, Observer<T> observer) {
        observable.subscribeOn(Schedulers.io())
                .compose(schedulersTransformer)
                .compose(new ErrorTransformer())
                .subscribe(observer);
    }

    private static class ErrorTransformer<T> implements ObservableTransformer {
        @Override
        public ObservableSource apply(Observable upstream) {
            return (Observable<T>) upstream.onErrorResumeNext(new HttpResponse<T>());
        }
    }

    public static class HttpResponse<T> implements Function<Throwable, Observable<T>> {
        @Override
        public Observable<T> apply(Throwable throwable) {
            return Observable.error(ExceptionHandler.handleException(throwable));
        }
    }
}
