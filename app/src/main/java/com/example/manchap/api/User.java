package com.example.manchap.api;

import com.example.manchap.api.model.Login;
import com.example.manchap.api.model.Signup;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface User {
    String BASE_URL = "/user";

    // Signup
    @PUT(BASE_URL + "/signup")
    Observable<com.example.manchap.model.User> signup(@Body Signup signup);

    // Login
    @POST(BASE_URL + "/login")
    Observable<com.example.manchap.model.User> login(@Body Login login);
}
