package com.example.manchap.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import com.example.manchap.R;
import com.example.manchap.api.BaseObserver;
import com.example.manchap.api.ExceptionHandler;
import com.example.manchap.dialog.LoadingDialog;
import com.example.manchap.model.User;
import com.example.manchap.util.Snackbar;
import com.example.manchap.viewmodel.SignupModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignupActivity extends AppCompatActivity {

    @BindView(R.id.et_name) AppCompatEditText name;
    @BindView(R.id.et_email) AppCompatEditText email;
    @BindView(R.id.et_pwd) AppCompatEditText password;
    @BindView(R.id.bt_signin) AppCompatButton signin;
    @BindView(R.id.bt_create) AppCompatButton create;

    private SignupModel signupModel;
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        initViews();
        initData();
    }

    private void initViews() {
        loadingDialog = LoadingDialog.newInstance();

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                signupModel.getName().setValue(s.toString().trim());
            }
        });


        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                signupModel.getEmail().setValue(s.toString().trim());
            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                signupModel.getPassword().setValue(s.toString().trim());
            }
        });

        signin.setOnClickListener(v -> {
            finish();
        });

        create.setOnClickListener(v -> {
            signupModel.signup(new BaseObserver<User>() {
                @Override
                public void hideDialog() {
                    loadingDialog.dismissAllowingStateLoss();
                }

                @Override
                public void showDialog() {
                    loadingDialog.show(getSupportFragmentManager(), loadingDialog.getTag());
                }

                @Override
                public void onError(ExceptionHandler.ResponseThrowable e) {
                    Snackbar.show(email, R.color.red, e.message);
                }

                @Override
                public void onResult(User result) {
                    // Notify
                    Snackbar.show(email, R.color.colorPrimaryDark, "Account created successfully");
                    // Done
                    finish();
                }
            });
        });
    }

    private void initData() {
        signupModel = new SignupModel();

        signupModel.getName().observe(this, s -> {
            checkEnableSignup();
        });

        signupModel.getEmail().observe(this, s -> {
            checkEnableSignup();
        });

        signupModel.getPassword().observe(this, s -> {
            checkEnableSignup();
        });
    }

    private void checkEnableSignup() {
        String name = signupModel.getName().getValue();
        String email = signupModel.getEmail().getValue();
        String pass = signupModel.getPassword().getValue();
        create.setEnabled(name != null && email != null && pass != null && name.length() > 0 && email.length() > 0 && pass.length() > 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loadingDialog != null && loadingDialog.isVisible()) {
            loadingDialog.dismissAllowingStateLoss();
        }
    }
}

