package com.example.manchap.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.manchap.R;
import com.example.manchap.adapter.PostsAdapter;
import com.example.manchap.api.BaseObserver;
import com.example.manchap.api.ExceptionHandler;
import com.example.manchap.api.model.Posts;
import com.example.manchap.dialog.LoadingDialog;
import com.example.manchap.model.Following;
import com.example.manchap.model.Post;
import com.example.manchap.module.AccountManager;
import com.example.manchap.module.FollowingCache;
import com.example.manchap.util.Snackbar;
import com.example.manchap.viewmodel.TimelineModel;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimelineActivity extends AppCompatActivity {

    public static final String KEY_USER = "USER";
    public static final int REQ_ACTIVITY_TIMELINE = 1;

    @BindView(R.id.sw_refresh) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.rv_posts) RecyclerView postsList;
    @BindView(R.id.cv_new_post) View newPostLayout;
    @BindView(R.id.et_text) AppCompatEditText editText;
    @BindView(R.id.bt_create) AppCompatButton createPost;
    @BindView(R.id.bt_logout) AppCompatButton logout;

    private AccountManager accountManager;
    private LoadingDialog loadingDialog;
    private LinearLayoutManager linearLayoutManager;
    private PostsAdapter postsAdapter;

    private TimelineModel timelineModel;
    private FollowingCache followingCache = FollowingCache.getInstance();
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);
        ButterKnife.bind(this);
        initData();
        initViews();
        refresh();
    }

    @Override
    public void onBackPressed() {
        // User Timeline
        if (userId != null) {
            setResult(RESULT_OK);
            finish();
        }
        // Global Timeline
        else {
            moveTaskToBack(false);
        }
    }

    private void initData() {
        // Get User if any
        userId = getIntent().getStringExtra(KEY_USER);

        // Init
        accountManager = AccountManager.getInstance();
        timelineModel = new TimelineModel(userId);

        // Observe
        timelineModel.getProcessing().observe(this, b -> {
            refreshLayout.setRefreshing(true);
        });

        timelineModel.getPostText().observe(this, text -> {
            checkEnableCreate();
        });
    }

    private void initViews() {
        this.getSupportActionBar().hide();

        loadingDialog = LoadingDialog.newInstance();

        newPostLayout.setVisibility(userId != null ? View.GONE : View.VISIBLE);

        refreshLayout.setOnRefreshListener(() -> {
            if (timelineModel.getProcessing().getValue()) {
                return;
            }
            // Fetch First Page
            refresh();
        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                timelineModel.getPostText().setValue(s.toString().trim());
            }
        });

        createPost.setOnClickListener(v -> {
            timelineModel.createPost(new BaseObserver<Post>() {
                @Override
                public void hideDialog() {
                    refreshLayout.setRefreshing(false);
                }

                @Override
                public void showDialog() {
                    refreshLayout.setRefreshing(true);
                }

                @Override
                public void onError(ExceptionHandler.ResponseThrowable e) {
                    Snackbar.show(refreshLayout, R.color.red, e.message);
                }

                @Override
                public void onResult(Post result) {
                    postsAdapter.addFront(result);
                    postsList.smoothScrollToPosition(0);
                    hideKeyboard();
                    editText.setText(null);
                }
            });
        });

        logout.setOnClickListener(v -> {
            accountManager.clear();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        });

        // Setup RecyclerView
        linearLayoutManager = new LinearLayoutManager(postsList.getContext(), RecyclerView.VERTICAL, false);
        postsAdapter = new PostsAdapter(postsList.getContext());
        postsAdapter.setClickListener(new PostsAdapter.ClickListener() {
            @Override
            public void onFollowClicked(int position, final Post post) {
                if (followingCache.get(post.user.id)) {
                    timelineModel.unfollow(post.user.id, new BaseObserver<Following>() {
                        @Override
                        public void hideDialog() {
                            loadingDialog.dismissAllowingStateLoss();
                        }

                        @Override
                        public void showDialog() {
                            loadingDialog.show(getSupportFragmentManager(), loadingDialog.getTag());
                        }

                        @Override
                        public void onError(ExceptionHandler.ResponseThrowable e) {
                            Snackbar.show(refreshLayout, R.color.red, e.message);
                        }

                        @Override
                        public void onResult(Following result) {
                            Snackbar.show(refreshLayout, R.color.colorPrimary, "Unfollowed " + post.user.name);
                            followingCache.set(post.user.id, false);
                            post.user.following.status = false;
                            post.user.following.data = null;
                            postsAdapter.notifyDataSetChanged();
                        }
                    });
                } else {
                    timelineModel.follow(post.user.id, new BaseObserver<Following>() {
                        @Override
                        public void hideDialog() {
                            loadingDialog.dismissAllowingStateLoss();
                        }

                        @Override
                        public void showDialog() {
                            loadingDialog.show(getSupportFragmentManager(), loadingDialog.getTag());
                        }

                        @Override
                        public void onError(ExceptionHandler.ResponseThrowable e) {
                            Snackbar.show(refreshLayout, R.color.red, e.message);
                        }

                        @Override
                        public void onResult(Following result) {
                            Snackbar.show(refreshLayout, R.color.colorPrimary, "Following " + post.user.name);
                            followingCache.set(post.user.id, true);
                            post.user.following.status = true;
                            post.user.following.data = result;
                            postsAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }

            @Override
            public void onProfileClicked(int position, Post post) {
                if (userId != null) {
                    return;
                }
                // Start Activity
                Intent intent = new Intent(getApplicationContext(), TimelineActivity.class);
                intent.putExtra(KEY_USER, post.user.id);
                startActivityForResult(intent, REQ_ACTIVITY_TIMELINE);
            }
        });
        postsList.setLayoutManager(linearLayoutManager);
        postsList.setAdapter(postsAdapter);
        postsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(postsAdapter.getItemCount() == linearLayoutManager.findLastCompletelyVisibleItemPosition() + 1) {
                    if(!refreshLayout.isRefreshing()) {
                        timelineModel.nextPage(new BaseObserver<Posts>() {
                            @Override
                            public void hideDialog() {
                                refreshLayout.setRefreshing(false);
                            }

                            @Override
                            public void showDialog() {
                                refreshLayout.setRefreshing(true);
                            }

                            @Override
                            public void onError(ExceptionHandler.ResponseThrowable e) {
                                Snackbar.show(refreshLayout, R.color.red, e.message);
                            }

                            @Override
                            public void onResult(Posts result) {
                                for (Post post : result.posts) {
                                    followingCache.set(post.user.id, post.user.following.status);
                                }
                                timelineModel.setPages(result.pages);
                                postsAdapter.addAll(result.posts);
                            }
                        });
                    }
                }
            }
        });
    }

    private void refresh() {
        timelineModel.firstPage(new BaseObserver<Posts>() {
            @Override
            public void hideDialog() {
                refreshLayout.setRefreshing(false);
            }

            @Override
            public void showDialog() {
                refreshLayout.setRefreshing(true);
            }

            @Override
            public void onError(ExceptionHandler.ResponseThrowable e) {
                Snackbar.show(refreshLayout, R.color.red, e.message);
            }

            @Override
            public void onResult(Posts result) {
                for (Post post : result.posts) {
                    followingCache.set(post.user.id, post.user.following.status);
                }
                timelineModel.setPages(result.pages);
                postsAdapter.clearAll();
                postsAdapter.addAll(result.posts);
            }
        });
    }

    private void checkEnableCreate() {
        String text = timelineModel.getPostText().getValue();
        createPost.setEnabled(text != null && text.length() > 0);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    @Override
    public void onActivityResult(int req, int res, Intent data) {
        switch (req) {
            case REQ_ACTIVITY_TIMELINE:
                postsAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loadingDialog != null && loadingDialog.isVisible()) {
            loadingDialog.dismissAllowingStateLoss();
        }
    }
}
