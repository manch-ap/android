package com.example.manchap.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.example.manchap.R;
import com.example.manchap.module.AccountManager;

import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    private AccountManager accountManager = AccountManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        this.getSupportActionBar().hide();

        // Wait for 2s
        new Handler().postDelayed(() -> {
            Intent intent;
            if (isUserLoggedIn()) {
                intent = new Intent(getApplicationContext(), TimelineActivity.class);
            } else {
                intent = new Intent(getApplicationContext(), LoginActivity.class);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }, 2000);
    }

    /*
     * Check if user is logged in
     */
    private boolean isUserLoggedIn() {
        return accountManager.getUser() != null;
    }
}
