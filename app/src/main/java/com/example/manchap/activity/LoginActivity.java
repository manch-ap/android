package com.example.manchap.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import com.example.manchap.R;
import com.example.manchap.api.BaseObserver;
import com.example.manchap.api.ExceptionHandler;
import com.example.manchap.dialog.LoadingDialog;
import com.example.manchap.model.User;
import com.example.manchap.module.AccountManager;
import com.example.manchap.util.Snackbar;
import com.example.manchap.viewmodel.LoginModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.et_email) AppCompatEditText email;
    @BindView(R.id.et_pwd) AppCompatEditText password;
    @BindView(R.id.bt_signin) AppCompatButton signin;
    @BindView(R.id.bt_signup) AppCompatButton signup;

    private LoginModel loginModel;
    private LoadingDialog loadingDialog;
    private AccountManager accountManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initViews();
        initData();
    }

    private void initData() {
        accountManager = AccountManager.getInstance();
        loginModel = new LoginModel();

        loginModel.getEmail().observe(this, s -> {
            checkEnableSignin();
        });

        loginModel.getPassword().observe(this, s -> {
            checkEnableSignin();
        });
    }

    private void initViews() {
        loadingDialog = LoadingDialog.newInstance();

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                loginModel.getEmail().setValue(s.toString().trim());
            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                loginModel.getPassword().setValue(s.toString().trim());
            }
        });

        signup.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
            startActivity(intent);
        });

        signin.setOnClickListener(v -> {
            loginModel.login(new BaseObserver<User>() {
                @Override
                public void hideDialog() {
                    loadingDialog.dismissAllowingStateLoss();
                }

                @Override
                public void showDialog() {
                    loadingDialog.show(getSupportFragmentManager(), loadingDialog.getTag());
                }

                @Override
                public void onError(ExceptionHandler.ResponseThrowable e) {
                    Snackbar.show(email, R.color.red, e.message);
                }

                @Override
                public void onResult(User result) {
                    // Set User and Continue with Main Activity
                    accountManager.setUser(result);
                    // Start Main Activity
                    Intent intent = new Intent(getApplicationContext(), TimelineActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
        });
    }

    private void checkEnableSignin() {
        String email = loginModel.getEmail().getValue();
        String pass = loginModel.getPassword().getValue();
        signin.setEnabled(email != null && pass != null && email.length() > 0 && pass.length() > 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loadingDialog != null && loadingDialog.isVisible()) {
            loadingDialog.dismissAllowingStateLoss();
        }
    }
}
